# apply-for-trusted-contributor

People who have been contributing to postmarketOS or similar free software
projects for the past six months may apply as
[Trusted Contributor](https://wiki.postmarketos.org/wiki/Team_members#Trusted_Contributors).

To do so, create a new issue in this repository and fill out the template.

Make sure to set it to confidential. The Core Team has access to these issues
and will decide whether to move forward with it.

### Tips for people who plan to become Trusted Contributors

* Give reviews/feedback on merge requests - even though you can't approve you can still help folks out!
* Be active in chat and gitlab issues, helping other users
