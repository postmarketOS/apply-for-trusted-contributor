*Thank you for applying as Trusted Contributor! Please fill out this template.*

### Who are the two TC / core team members that endorse your application?

<!-- You should have spoken to them and discussed endorsement prior to opening an issue here! -->

### What are your reasons for getting involved in postmarketOS and for applying to become a TC?


### How long have you been working on postmarketOS or similar projects? (which ones)


### What did you do in pmOS so far? Please provide examples / supporting proof.

<!--
Some examples of valued contributions include (but are not limited to):
   - submitting patches to projects under postmarketOS
   - providing support to others on the issue tracker or chat rooms
   - performing code reviews
   - creating / improving device ports
   - submitting artwork, writing blog posts, giving talks / promoting pmOS
   - making accessibility improvements
-->

### What do you plan to do in pmOS in the near future?


### Checkboxes

*Everybody contributing to postmarketOS should have read the CoC and act*
*according to it. This is especially important for Trusted Contributors.*
*You should be familiar with the other documents as well.*

- [ ] I have read the [Code of Conduct](https://postmarketos.org/coc)
- [ ] I have read the [device categorization rules](https://wiki.postmarketos.org/wiki/Device_categorization)
- [ ] I have read the [rules for merging](https://wiki.postmarketos.org/wiki/Rules_for_merging)
- [ ] I have enabled [2FA](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html#enable-two-factor-authentication)

*The following should instruct gitlab to mark the issue as confidential. It*
*worked when we tested it. To be extra sure, click the checkbox in the issue*
*to mark it as confidential.*

/confidential
